#####
# IAM
#####

output "task_definition_iam_execution_role_arn" {
  value = var.cluster_execution_role_enabled ? try(aws_iam_role.execution_role.*.arn, [""]) : null
}

#####
# Cluster
#####

output "cluster_arn" {
  value = var.cluster_enabled ? try(aws_ecs_cluster.this.*.arn, [""]) : null
}

output "cluster_id" {
  value = var.cluster_enabled ? try(aws_ecs_cluster.this.*.id, [""]) : null
}

output "cluster_name" {
  value = var.cluster_enabled ? try(aws_ecs_cluster.this.*.name, [""]) : null
}

#####
# Task definition
#####

output "task_definition_arn" {
  value = var.task_definition_enabled ? try(aws_ecs_task_definition.this.*.arn, [""]) : null
}

output "task_definition_revision" {
  value = var.task_definition_enabled ? try(aws_ecs_task_definition.this.*.revision, [""]) : null
}

output "task_definition_container_definition_json" {
  value = var.task_definition_enabled ? try(aws_ecs_task_definition.this.*.container_definitions, [""]) : null
}

#####
# Service
#####

output "service_id" {
  value = var.task_definition_enabled ? try(aws_ecs_service.this.*.id, [""]) : null
}

output "service_name" {
  value = var.task_definition_enabled ? try(aws_ecs_service.this.*.name, [""]) : null
}

output "service_cluster" {
  value = var.task_definition_enabled ? try(aws_ecs_service.this.*.cluster, [""]) : null
}

output "service_desired_count" {
  value = var.task_definition_enabled ? try(aws_ecs_service.this.*.desired_count, [""]) : null
}

output "service_iam_role_arn" {
  value = var.task_definition_enabled ? try(aws_ecs_service.this.*.iam_role, [""]) : null
}
