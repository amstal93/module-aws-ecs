variable "prefix" {
  description = "Prefix to be added to with all resource's names of the module. Prefix is mainly used for tests and should remain empty in normal circumstances"
  type        = string
  default     = ""
}

#####
# IAM
#####

variable "cluster_execution_role_enabled" {
  description = "Whether or not to create the execution role"
  type        = bool
  default     = true
}

variable "cluster_execution_policy_name" {
  description = "IAM policy name that allows your Amazon ECS container task to make calls to other AWS services"
  type        = string
  default     = "execution_policy"

  validation {
    condition     = can(regex("^[\\w+=,.@-]{1,128}$", var.cluster_execution_policy_name))
    error_message = "The var.cluster_execution_policy_name must match “^[\\w+=,.@-]{1,128}$”."
  }
}

variable "cluster_execution_policy_tags" {
  description = "Tags to be used for the Execution Policy. Will be merged with `var.tags`"
  type        = map(string)
  default     = {}
}

variable "cluster_execution_role_name" {
  description = "Execution role's name"
  type        = string
  default     = "execution_role"

  validation {
    condition     = can(regex("^[\\w+=,.@-]{1,128}$", var.cluster_execution_role_name))
    error_message = "The var.cluster_execution_role_name must match “^[\\w+=,.@-]{1,128}$”."
  }
}

variable "cluster_execution_role_tags" {
  description = "Tags to be used for the Execution role. Will be merged with `var.tags`"
  type        = map(string)
  default     = {}
}

#####
# Cluster
#####

variable "cluster_enabled" {
  description = "Controls if ECS should be created"
  type        = bool
  default     = true
}

variable "cluster_name" {
  description = "ECS cluster name"
  type        = string
  default     = ""
}

variable "cluster_setting" {
  description = <<-DOCUMENTATION
  Configuration block(s) with cluster settings
  name = Name of the setting to manage. Valid values: `containerInsights`.
  value = The value to assign to the setting. Value values are `enabled` and `disabled`.
  DOCUMENTATION
  type = list(object({
    name  = string
    value = string
  }))
  default = []

  validation {
    condition = alltrue([
      for o in var.cluster_setting : contains("containerInsights", o.name)
    ])
    error_message = "The cluster_setting name should be `containerInsights`."
  }

  validation {
    condition = alltrue([
      for o in var.cluster_setting : contains(["enabled", "disabled"], o.value)
    ])
    error_message = "The cluster_setting value should be from this list: [`enabled`, `disabled`]."
  }
}

variable "cluster_configuration" {
  description = <<-DOCUMENTATION
  The execute command configuration for the cluster. See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_cluster#configuration for more details.
  * kms_key_id            (Optional, string) : The AWS Key Management Service key ID to encrypt the data between the local client and the container.
  * logging               (Optional, string) :  The log setting to use for redirecting logs for your execute command results. Valid values are `NONE`, `DEFAULT`, and `OVERRIDE`.
  * use_log_configuration (Optional, bool) : Whether or not to use log configuration.
    * cloud_watch_encryption_enabled (Optional, bool) : Whether or not to enable encryption on the CloudWatch logs. If not specified, encryption will be disabled.
    * cloud_watch_log_group_name     (Optional, string) : The name of the CloudWatch log group to send logs to.
    * s3_bucket_name                 (Optional, string) : The name of the S3 bucket to send logs to.
    * s3_bucket_encryption_enabled   (Optional, bool) : Whether or not to enable encryption on the logs sent to S3. If not specified, encryption will be disabled.
    * s3_key_prefix                  (Optional, string) : An optional folder in the S3 bucket to place logs in.
  DOCUMENTATION
  type = list(object({
    kms_key_id            = optional(string)
    logging               = optional(string)
    use_log_configuration = optional(bool)
    log_configuration = optional(object({
      cloud_watch_encryption_enabled = optional(bool)
      cloud_watch_log_group_name     = optional(string)
      s3_bucket_name                 = optional(string)
      s3_bucket_encryption_enabled   = optional(bool)
      s3_key_prefix                  = optional(string)
    }))
  }))
  default = []
}

variable "cluster_tags" {
  description = "Tags to be used for the Cluster. Will be merged with `var.tags`"
  type        = map(string)
  default     = {}
}

variable "cluster_capacity_providers" {
  description = "List of short names of one or more capacity providers to associate with the cluster. Valid values also include `FARGATE` and `FARGATE_SPOT`."
  type        = list(string)
  default     = []

  validation {
    condition = alltrue([
      for o in var.cluster_capacity_providers : contains(["FARGATE", "FARGATE_SPOT"], o)
    ])
    error_message = "The object cluster_capacity_providers should be a list of string from this list: FARGATE, FARGATE_SPOT."
  }
}

variable "cluster_default_capacity_provider_strategy" {
  description = <<-DOCUMENTATION
  The capacity provider strategy to use by default for the cluster. Can be one or more.
  capacity_provider (Required, string) : Name of the capacity provider
  weight (Optional, number) : The relative percentage of the total number of launched tasks that should use the specified capacity provider. The `weight` value is taken into consideration after the base count of tasks has been satisfied. Defaults to `0`.
  base (Optional, number) : The number of tasks, at a minimum, to run on the specified capacity provider. Only one capacity provider in a capacity provider strategy can have a base defined. Defaults to `0`.
  DOCUMENTATION
  type = list(object({
    capacity_provider = string
    weight            = optional(number)
    base              = optional(number)
  }))
  default = []
}

variable "tags" {
  description = "A general map of tags which will be merged with others."
  type        = map(string)
  default     = {}
}

#####
# aws_ecs_task_definition
#####

variable "task_definition_enabled" {
  description = "Whether or not to create the task definition"
  type        = bool
  default     = false
}

variable "task_definition_name" {
  description = "Task definition name us in `family` variable"
  type        = string
  default     = null
}

variable "task_definition_use_external_execution_role" {
  description = "Whether or not to use external execution role"
  type        = bool
  default     = false
}

variable "task_definition_external_execution_role_arn" {
  description = "The Amazon Resource Name (ARN) of the task execution role that grants the Amazon ECS container agent permission to make AWS API calls on your behalf."
  type        = string
  default     = ""
}

variable "task_definition_use_external_task_role" {
  description = "Whether or not to use external task role"
  type        = bool
  default     = false
}

variable "task_definition_external_task_role_arn" {
  description = "When you register a task definition, you can provide a task role for an IAM role that allows the containers in the task permission to call the AWS APIs that are specified in its associated policies on your behalf."
  type        = string
  default     = ""
}

variable "task_definition_container_definitions" {
  description = <<-DOCUMENTATION
  Container definitions parameters.
  https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#container_definitions for more details.
  * command   (Optional, list (string)) : The command that is passed to the container.
  * cpu       (Optional but required if FARGATE, number) : The number of cpu units the Amazon ECS container agent will reserve for the container.
  * dependsOn (Optional, list(object)) : The dependencies defined for container startup and shutdown. A container can contain multiple dependencies.
    * containerName (Required, string) : The container name that must meet the specified condition.
    * condition     (Optional, string) : The dependency condition of the container.
  * disableNetworking     (Optional, bool) : When this parameter is true, networking is off within the container.
* dnsSearchDomains   (Optional, list (string)) : A list of DNS search domains that are presented to the container.
  * dnsServers            (Optional, list (string)) : A list of DNS servers that are presented to the container.
  * dockerLabels          (Optional, map (string)) : A key/value map of labels to add to the container.
  * dockerSecurityOptions (Optional, list (string)) : A list of strings to provide custom labels for SELinux and AppArmor multi-level security systems.
  * entryPoint            (Optional, list (string)) : The entry point that is passed to the container.
  * environment           (Optional, list(object)) : The environment variables to pass to a container.
    * name  (Required if environment is used, string) : The name of the environment variable.
    * value (Required if environment is used, string) : The value of the environment variable.
  * environmentFiles (Optional, list(object)) : A list of files containing the environment variables to pass to a container.
    * type  (Required if environmentFiles is used, string) : The file type to use. The only supported value is s3.
    * value (Required if environmentFiles is used, string) : The Amazon Resource Name (ARN) of the Amazon S3 object containing the environment variable file.
  * essential  (Optional, bool) : If the essential parameter of a container is marked as true, and that container fails or stops for any reason, all other containers that are part of the task are stopped. If the essential parameter of a container is marked as false, then its failure does not affect the rest of the containers in a task. If this parameter is omitted, a container is assumed to be essential.
  * extraHosts (Optional, list(object)) : A list of hostnames and IP address mappings to append to the /etc/hosts file on the container.
    * hostname  (Required if extraHosts is used, string) : The hostname to use in the /etc/hosts entry.
    * ipAddress (Required if extraHosts is used, string) : The IP address to use in the /etc/hosts entry.
  * firelensConfiguration (Optional, object) : The FireLens configuration for the container. This is used to specify and configure a log router for container logs.
    * options (Optional, string) : The options to use when configuring the log router. This field is optional and can be used to specify a custom configuration file or to add additional metadata, such as the task, task definition, cluster, and container instance details to the log event.
    * type    (Required, string) : The log router to use. The valid values are `fluentd` or `fluentbit`.
  * healthCheck (Optional, object) : The container health check command and associated configuration parameters for the container.
    * command (Optional, list (string)) : A string array representing the command that the container runs to determine if it is healthy.
    * interval  (Optional, number) : The time period in seconds between each health check execution. You may specify between 5 and 300 seconds. The default value is 30 seconds.
    * retries    (Optional, number) : The number of times to retry a failed health check before the container is considered unhealthy. You may specify between 1 and 10 retries. The default value is 3 retries.
    * startPeriod (Optional, number) : The optional grace period within which to provide containers time to bootstrap before failed health checks count towards the maximum number of retries. You may specify between 0 and 300 seconds. The `startPeriod` is disabled by default.
    * timeout  (Optional, number) : The time period in seconds to wait for a health check to succeed before it is considered a failure. You may specify between 2 and 60 seconds. The default value is 5 seconds.
  * hostname    (Optional, string) : The hostname to use for your container.
  * image       (Required, string) : The image used to start a container. This string is passed directly to the Docker daemon.
  * interactive (Optional, bool) : When this parameter is true, this allows you to deploy containerized applications that require stdin or a tty to be allocated.
  * links (Optional, list (string)) : The link parameter allows containers to communicate with each other without the need for port mappings. Only supported if the network mode of a task definition is set to bridge.
  * linuxParameters (Optional, object) : Linux-specific options that are applied to the container, such as KernelCapabilities.
    * capabilities (Optional, object) : The Linux capabilities for the container that are added to or dropped from the default configuration provided by Docker.
      * add   (Optional, list (string)) : The Linux capabilities for the container to add to the default configuration provided by Docker.
      * drop  (Optional, list (string)) : The Linux capabilities for the container to remove from the default configuration provided by Docker.
    * devices (Optional, list(object)) : Any host devices to expose to the container.
      * containerPath (Optional, string) : The path inside the container at which to expose the host device.
      * hostPath      (Required, string) : The path for the device on the host container instance.
      * permissions   (Optional, list (string)) : The explicit permissions to provide to the container for the device. By default, the container has permissions for `read`, `write`, and `mknod` on the device.
    * initProcessEnabled (Optional, bool) : Run an init process inside the container that forwards signals and reaps processes.
    * maxSwap            (Optional, number) : The total amount of swap memory (in MiB) a container can use.
    * sharedMemorySize   (Optional, number) : The value for the size (in MiB) of the /dev/shm volume.
    * swappiness         (Optional, number) : This allows you to tune a container's memory swappiness behavior.
    * tmpfs              (Optional, list(object)) : The container path, mount options, and maximum size (in MiB) of the tmpfs mount.
      * containerPath (Required, string) : The absolute file path where the tmpfs volume is to be mounted.
      * mountOptions (Optional, list (string)) : The list of tmpfs volume mount options.
      * size (Required, number) : The maximum size (in MiB) of the tmpfs volume.
  * logConfiguration (Optional, object) : The log configuration specification for the container.
    * logDriver     (Required, string) : The log driver to use for the container. The valid values listed earlier are log drivers that the Amazon ECS container agent can communicate with by default.
    * options       (Optional, map(string)) : The configuration options to send to the log driver.
    * secretOptions (Optional, list(object)) : An object representing the secret to pass to the log configuration.
      * name      (Required, string) : The value to set as the environment variable on the container.
      * valueFrom (Required, string) : The secret to expose to the log configuration of the container.
  * memory            (Optional, number) : The amount (in MiB) of memory to present to the container. If using the Fargate launch type, this parameter is optional. If using the EC2 launch type, you must specify either a task-level memory value or a container-level memory value.
  * memoryReservation (Optional, number) : The soft limit (in MiB) of memory to reserve for the container.
  * mountPoints       (Optional, list(object)) : The mount points for data volumes in your container.
  * containerPath     (Required if mountPoints is used, string) : The path on the container to mount the volume at.
    * readOnly     (Required if mountPoints is used, bool) : If this value is `true`, the container has read-only access to the volume. If this value is `false`, then the container can write to the volume. The default value is `false`.
    * sourceVolume (Required if mountPoints is used, string) : The name of the volume to mount.
  * name (Required, string) : The name of a container. Up to 255 letters (uppercase and lowercase), numbers, hyphens, and underscores are allowed.
  * portMappings (Optional, list(object)) : Port mappings allow containers to access ports on the host container instance to send or receive traffic.
    * containerPort (Optional, number) : The port number on the container that is bound to the user-specified or automatically assigned host port.
    * hostPort      (Optional, number) : The port number on the container instance to reserve for your container.
    * protocol      (Optional, string) : The protocol used for the port mapping. Valid values are tcp and udp. The default is tcp.
  * privileged             (Optional, bool) : When this parameter is `true`, the container is given elevated privileges on the host container instance (similar to the root user).
  * pseudoTerminal         (Optional, bool) : When this parameter is `true`, a TTY is allocated.
  * readonlyRootFilesystem (Optional, bool) : When this parameter is `true`, the container is given read-only access to its root file system.
  * repositoryCredentials  (Optional, object) : The private repository authentication credentials to use.
    * credentialsParameter (Required, string) : The Amazon Resource Name (ARN) of the secret containing the private repository credentials.
  * resourceRequirements = (Optional, list(object)) :
    * type  (Required, string) : The type of resource to assign to a container. The supported values are GPU or InferenceAccelerator.
    * value (Required, string) : The value for the specified resource type. See : https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_ResourceRequirement.html
  * secrets (Optional, list(object)) : The secrets to pass to the container.
    * name      (Required, string) : The name of the secret.
    * valueFrom (Required, string) : The secret to expose to the container. The supported values are either the full ARN of the AWS Secrets Manager secret or the full ARN of the parameter in the SSM Parameter Store.
  * startTimeout (Optional, number) : Time duration (in seconds) to wait before giving up on resolving dependencies for a container.
  * stopTimeout  (Optional, number) : Time duration (in seconds) to wait before the container is forcefully killed if it doesn't exit normally on its own.
  * systemControls (Optional, list(object)) : A list of namespaced kernel parameters to set in the container.
    * namespace (Optional, string) : The namespaced kernel parameter to set a `value` for.
    * value     (Optional, string) : The value for the namespaced kernel parameter that's specified in `namespace`.
  * ulimits (Optional, list(object)) : A list of `ulimits` to set in the container. If a `ulimit` value is specified in a task definition, it overrides the default values set by Docker.
    * hardLimit (Required, number) : The hard limit for the `ulimit` type.
    * name      (Required, string) : The type of the `ulimit`.
    * softLimit (Required, number) : The soft limit for the `ulimit` type.
  * user (Optional, string) : The user to use inside the container.
  * volumesFrom (Optional, list(object)) : Data volumes to mount from another container.
    * readOnly       (Optional, bool) : If this value is `true`, the container has read-only access to the volume. If this value is `false`, then the container can write to the volume. The default value is `false`.
    * sourceContainer (Optional, string) : The name of another container within the same task definition to mount volumes from.
  * workingDirectory (Optional, string) : The working directory to run commands inside the container in.
  DOCUMENTATION
  type = list(object({
    command = optional(list(string))
    cpu     = optional(number)
    dependsOn = optional(list(object({
      containerName = string
      condition     = optional(string)
    })))
    disableNetworking     = optional(bool)
    dnsSearchDomains      = optional(list(string))
    dnsServers            = optional(list(string))
    dockerLabels          = optional(map(string))
    dockerSecurityOptions = optional(list(string))
    entryPoint            = optional(list(string))
    environment = optional(list(object({
      name  = string
      value = string
    })))
    environmentFiles = optional(list(object({
      type  = string
      value = string
    })))
    essential = optional(bool)
    extraHosts = optional(list(object({
      hostname  = string
      ipAddress = string
    })))
    firelensConfiguration = optional(object({
      options = optional(string)
      type    = string
    }))
    healthCheck = optional(object({
      command     = list(string)
      interval    = optional(number)
      retries     = optional(number)
      startPeriod = optional(number)
      timeout     = optional(number)
    }))
    hostname    = optional(string)
    image       = string
    interactive = optional(bool)
    links       = optional(list(string))
    linuxParameters = optional(object({
      capabilities = optional(object({
        add  = optional(list(string))
        drop = optional(list(string))
      }))
      devices = optional(list(object({
        containerPath = optional(string)
        hostPath      = string
        permissions   = optional(list(string))
      })))
      initProcessEnabled = optional(bool)
      maxSwap            = optional(number)
      sharedMemorySize   = optional(number)
      swappiness         = optional(number)
      tmpfs = optional(list(object({
        containerPath = string
        mountOptions  = optional(list(string))
        size          = number
      })))
    }))
    logConfiguration = optional(object({
      logDriver = string
      options   = optional(map(string))
      secretOptions = optional(list(object({
        name      = string
        valueFrom = string
      })))
    }))
    memory            = optional(number)
    memoryReservation = optional(number)
    mountPoints = optional(list(object({
      containerPath = optional(string)
      readOnly      = optional(bool)
      sourceVolume  = optional(string)
    })))
    name = string
    portMappings = optional(list(object({
      containerPort = optional(number)
      hostPort      = optional(number)
      protocol      = optional(string)
    })))
    privileged             = optional(bool)
    pseudoTerminal         = optional(bool)
    readonlyRootFilesystem = optional(bool)
    repositoryCredentials = optional(object({
      credentialsParameter = string
    }))
    resourceRequirements = optional(list(object({
      type  = string
      value = string
    })))
    secrets = optional(list(object({
      name      = string
      valueFrom = string
    })))
    startTimeout = optional(number)
    stopTimeout  = optional(number)
    systemControls = optional(list(object({
      namespace = optional(string)
      value     = optional(string)
    })))
    ulimits = optional(list(object({
      hardLimit = number
      name      = string
      softLimit = number
    })))
    user = optional(string)
    volumesFrom = optional(list(object({
      readOnly        = optional(bool)
      sourceContainer = optional(string)
    })))
    workingDirectory = optional(string)
  }))
  default = []
}

variable "task_definition_network_mode" {
  description = "Docker networking mode to use for the containers in the task."
  type        = string
  default     = "awsvpc"

  validation {
    condition     = can(regex("awsvpc|none|bridge|host", var.task_definition_network_mode))
    error_message = "“var.task_definition_network_mode” does not match awsvpc, none, bridge or host."
  }
}

variable "task_definition_fargate_cpu" {
  description = "CPU size for fargate, please refer to https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-cpu-memory-error.html"
  default     = 1024
  type        = number
}

variable "task_definition_fargate_mem" {
  description = "Memory to use for fargate"
  default     = 2048
  type        = number
}

variable "task_definition_requires_compatibilities" {
  description = "ARN of the role to assign to the launched container"
  type        = list(any)
  default     = []

  validation {
    condition     = var.task_definition_requires_compatibilities == null || !contains([for item in var.task_definition_requires_compatibilities : contains(["EC2", "FARGATE", "EXTERNAL"], item)], false)
    error_message = "The variable requires_compatibilities contain non-valid value(s) (EC2, FARGATE or EXTERNAL)."
  }
}

variable "task_definition_volumes" {
  description = <<-DOCUMENTATION
  Configuration block for volumes that containers in your task may use.
  * name (Required, string) : The name of the volume. Up to 255 letters (uppercase and lowercase), numbers, hyphens, and underscores are allowed. This name is referenced in the sourceVolume parameter of container definition mountPoints.
  * host_path (Optional, string) :  Path on the host container instance that is presented to the container. If not set, ECS will create a nonpersistent data volume that starts empty and is deleted after the task has finished.
  * use_docker_volume_configuration (Required, bool) : Whether or not to use docker volume configuration
  * docker_volume_configuration (Optional, object) :  Configuration block to configure a docker volume.
    * autoprovision = (Optional, bool) : If this value is `true`, the Docker volume is created if it does not already exist. Note: This field is only used if the scope is `shared`.
    * driver        (Optional, string) : Docker volume driver to use. The driver value must match the driver name provided by Docker because it is used for task placement.
    * driver_opts   (Optional, string) : Map of Docker driver specific options.
    * labels        (Optional, string) : Custom metadata to add to your Docker volume.
    * scope         (Optional, string) : The scope for the Docker volume, which determines its lifecycle. Docker volumes that are scoped to a task are automatically provisioned when the task starts destroyed when the task is cleaned up. Docker volumes that are scoped as shared persist after the task stops.
  * use_efs_volume_configuration (Required, bool) : Whether or not to use efs volume configuration
    * file_system_id  (Required, string) : ID of the EFS File System.
    * root_directory      (Optional, string) :  Directory within the Amazon EFS file system to mount as the root directory inside the host. If this parameter is omitted, the root of the Amazon EFS volume will be used.
    * transit_encryption   (Optional, string) : Whether or not to enable encryption for Amazon EFS data in transit between the Amazon ECS host and the Amazon EFS server. Transit encryption must be enabled if Amazon EFS IAM authorization is used. Valid values: `ENABLED`, `DISABLED`. If this parameter is omitted, the default value of `DISABLED` is used.
    * transit_encryption_port  (Optional, number) :  Port to use for transit encryption. If you do not specify a transit encryption port, it will use the port selection strategy that the Amazon EFS mount helper uses.
    * use_authorization_config (Optional, bool) : Whether or not to use `authorization_config`
    * authorization_config (Optional, object) : Configuration block for authorization for the Amazon EFS file system.
      * access_point_id (Optional, string) : Access point ID to use. If an access point is specified, the root directory value will be relative to the directory set for the access point. If specified, transit encryption must be enabled in the EFSVolumeConfiguration.
      * iam             (Optional, string) :  Whether or not to use the Amazon ECS task IAM role defined in a task definition when mounting the Amazon EFS file system. If enabled, transit encryption must be enabled in the EFSVolumeConfiguration. Valid values: `ENABLED`, `DISABLED`. If this parameter is omitted, the default value of `DISABLED` is used.
    * use_fsx_windows_file_server_volume_configuration (Required, bool) : Whether or not to use `fsx_windows_file_server_volume_configuration`.
    * fsx_windows_file_server_volume_configuration (Optional, object) : Configuration block for an FSX Windows File Server volume.
      * file_system_id         (Optional, string) : The Amazon FSx for Windows File Server file system ID to use.
      * root_directory         (Optional, string) : The directory within the Amazon FSx for Windows File Server file system to mount as the root directory inside the host.
      * credentials_parameter  (Required, string) : The authorization credential option to use. The authorization credential options can be provided using either the Amazon Resource Name (ARN) of an AWS Secrets Manager secret or AWS Systems Manager Parameter Store parameter.
      * domain                 (Required, string) :  A fully qualified domain name hosted by an AWS Directory Service Managed Microsoft AD (Active Directory) or self-hosted AD on Amazon EC2.
  DOCUMENTATION
  type = list(object({
    name      = string
    host_path = optional(string)

    use_docker_volume_configuration = bool

    docker_volume_configuration = optional(object({
      autoprovision = optional(bool)
      driver        = optional(string)
      driver_opts   = optional(string)
      labels        = optional(string)
      scope         = optional(string)
    }))

    use_efs_volume_configuration = bool

    efs_volume_configuration = optional(object({
      file_system_id           = string
      root_directory           = optional(string)
      transit_encryption       = optional(string)
      transit_encryption_port  = optional(number)
      use_authorization_config = bool
      authorization_config = optional(object({
        access_point_id = optional(string)
        iam             = optional(string)
      }))
    }))

    use_fsx_windows_file_server_volume_configuration = bool

    fsx_windows_file_server_volume_configuration = optional(object({
      file_system_id        = optional(string)
      root_directory        = optional(string)
      credentials_parameter = string
      domain                = string
    }))

  }))
  default = []
}

variable "task_definition_placement_constraints" {
  description = "Placement constraints definition block where you can group container instances by attributes."
  type = list(object({
    type       = string
    expression = optional(string)
  }))
  default = []
}

variable "task_definition_proxy_configuration" {
  description = <<-DOCUMENTATION
  Proxy configuration definition block
  * container_name (Required, string) : Name of the container that will serve as the App Mesh proxy.
  * type (Optional, object) : Proxy type. The default value is `APPMESH`. The only supported value is `APPMESH`.
  * properties (Required, list(object)) : details int : https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#proxyConfiguration
    * name  (Optional, string) : The name of the key-value pair.
    * value (Optional, string) : The value of the key-value pair.
  DOCUMENTATION
  type = list(object({
    container_name = string
    properties = list(object({
      name  = optional(string)
      value = optional(string)
    }))
    type = optional(string)
  }))
  default = []
}

variable "task_definition_ephemeral_storage" {
  description = <<-DOCUMENTATION
  The amount of ephemeral storage in GB to allocate for the task.
  This parameter is used to expand the total amount of ephemeral storage available, beyond the default amount, for tasks hosted on AWS Fargate.
  DOCUMENTATION
  type = list(object({
    size_in_gib = number
  }))
  default = []
}

variable "task_definition_inference_accelerator" {
  description = <<-DOCUMENTATION
  Inference accelerator definition
  device_name  (Required, string) : Elastic Inference accelerator device name. The deviceName must also be referenced in a container definition as a ResourceRequirement.
  device_type  (Required, string) : Elastic Inference accelerator type to use.
  DOCUMENTATION
  type = list(object({
    device_name = string
    device_type = string
  }))
  default = []
}

variable "task_definition_runtime_platform" {
  description = <<-DOCUMENTATION
  Runtime platform definition
  operating_system_family (Optional, string) : If the requires_compatibilities is FARGATE this field is required; must be set to a valid option from the operating system family in the runtime platform setting
  cpu_architecture        (Optional, string) : Must be set to either `X86_64` or `ARM64`
  DOCUMENTATION
  type = list(object({
    operating_system_family = optional(string)
    cpu_architecture        = optional(string)
  }))
  default = []
}

variable "task_definition_ipc_mode" {
  description = <<-DOCUMENTATION
  The IPC resource namespace to use for the containers in the task. The valid values are `host`, `task`, or `none`.
  This parameter is not supported for Windows containers or tasks using the Fargate launch type.
  DOCUMENTATION
  type        = string
  default     = null
}

variable "task_definition_pid_mode" {
  description = <<-DOCUMENTATION
  The process namespace to use for the containers in the task. The valid values are `host` or `task`.
  This parameter is not supported for Windows containers or tasks using the Fargate launch type.
  DOCUMENTATION
  type        = string
  default     = null
}

variable "task_definition_tags" {
  description = "Tags to be used for the task_definition. Will be merged with `var.tags`"
  type        = map(string)
  default     = {}
}

#####
# aws_ecs_service
#####

variable "service_enabled" {
  description = "Whether or not to create ecs service"
  type        = bool
  default     = false
}

variable "service_name" {
  description = "Name of the service (up to 255 letters, numbers, hyphens, and underscores)"
  type        = string
  default     = ""
}

variable "service_capacity_provider_strategy" {
  description = "Capacity provider strategies to use for the service. Can be one or more."
  type = list(object({
    base              = optional(number)
    capacity_provider = string
    weight            = number
  }))
  default = []
}

variable "service_deployment_circuit_breaker" {
  description = <<-DOCUMENTATION
Configuration block for deployment circuit breaker.
enable: Whether to enable the deployment circuit breaker logic for the service.
rollback: Whether to enable Amazon ECS to roll back the service if a service deployment fails. If rollback is enabled, when a service deployment fails, the service is rolled back to the last deployment that completed successfully.
DOCUMENTATION
  type = list(object({
    enable   = bool
    rollback = bool
  }))
  default = []
}

variable "service_deployment_controller" {
  description = <<-DOCUMENTATION
  Configuration block for deployment controller configuration.
  Type of deployment controller.  Valid values: `CODE_DEPLOY`, `ECS`, `EXTERNAL`. Default: `ECS`.
DOCUMENTATION
  type = list(object({
    type = optional(string)
  }))
  default = [
    {
      type = "ECS"
    }
  ]

  validation {
    condition = alltrue([
      for o in var.service_deployment_controller : contains(["CODE_DEPLOY", "ECS", "EXTERNAL"], o.type)
    ])
    error_message = "The object deployment_controller should be a string from this list: CODE_DEPLOY, ECS or EXTERNAL."
  }
}
variable "service_deployment_maximum_percent" {
  description = "Upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can be running in a service during a deployment. Not valid when using the DAEMON scheduling strategy."
  type        = number
  default     = null
}

variable "service_deployment_minimum_healthy_percent" {
  description = "Lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must remain running and healthy in a service during a deployment."
  type        = number
  default     = null
}

variable "service_desired_count" {
  description = <<-DOCUMENTATION
  Number of instances of the task definition to place and keep running.
  Defaults to 0 if not declared. Do not specify if using the DAEMON scheduling strategy.
  DOCUMENTATION
  type        = number
  default     = null
}

variable "service_enable_ecs_managed_tags" {
  description = "Specifies whether to enable Amazon ECS managed tags for the tasks within the service."
  type        = bool
  default     = null
}

variable "service_enable_execute_command" {
  description = "Specifies whether to enable Amazon ECS Exec for the tasks within the service."
  type        = bool
  default     = null
}

variable "service_force_new_deployment" {
  description = <<-DOCUMENTATION
  Enable to force a new task deployment of the service.
  This can be used to update tasks to use a newer Docker image with same image/tag combination (e.g., myimage:latest),
  roll Fargate tasks onto a newer platform version, or immediately deploy ordered_placement_strategy and placement_constraints updates.
  DOCUMENTATION
  type        = bool
  default     = null
}

variable "service_health_check_grace_period_seconds" {
  description = <<-DOCUMENTATION
  Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature shutdown, up to 2147483647.
  Only valid for services configured to use load balancers.
  DOCUMENTATION
  type        = number
  default     = null

  validation {
    condition     = var.service_health_check_grace_period_seconds == null || coalesce(var.service_health_check_grace_period_seconds, 10293) <= 2147483647
    error_message = "`var.service_health_check_grace_period_seconds` have to be under or equal to `2147483647`."
  }
}

variable "service_iam_role" {
  description = <<-DOCUMENTATION
  ARN of the IAM role that allows Amazon ECS to make calls to your load balancer on your behalf.
  This parameter is required if you are using a load balancer with your service, but only if your task definition does not use the awsvpc network mode. If using awsvpc network mode, do not specify this role.
  DOCUMENTATION
  type        = string
  default     = null

  validation {
    condition     = can(regex("^arn:aws:iam::[[:digit:]]{12}:role/.+", var.service_iam_role)) || var.service_iam_role == null
    error_message = "Must be a valid AWS IAM role ARN, which match this regex: `^arn:aws:iam::[[:digit:]]{12}:role/.+ ."
  }
}

variable "service_launch_type" {
  description = "Launch type on which to run your service. The valid values are EC2, FARGATE, and EXTERNAL. Defaults to EC2."
  type        = string
  default     = "EC2"

  validation {
    condition     = can(regex("EC2|FARGATE|EXTERNAL", var.service_launch_type))
    error_message = "“var.service_launch_type” does not match EC2, FARGATE or EXTERNAL."
  }
}

variable "service_load_balancer" {
  description = <<-DOCUMENTATION
  Configuration block for load balancers
  target_group_arn (Required, string) : ARN of the Load Balancer target group to associate with the service.
  container_name (Required, string) : Name of the container to associate with the load balancer (as it appears in a container definition).
  container_port (Required, number) : Port on the container to associate with the load balancer.
  DOCUMENTATION
  type = list(object({
    target_group_arn = string
    container_name   = string
    container_port   = number
  }))
  default = []
}

variable "service_network_configuration" {
  description = <<-DOCUMENTATION
  Network configuration for the service. This parameter is required for task definitions that use the awsvpc network mode to receive their own Elastic Network Interface, and it is not supported for other network modes.
  subnets (Required, list(string)) : Subnets associated with the task or service.
  security_groups (Optional, list(string)) : Security groups associated with the task or service. If you do not specify a security group, the default security group for the VPC is used.
  assign_public_ip (Optional, bool) : Assign a public IP address to the ENI (Fargate launch type only). Valid values are `true` or `false`. Default `false`.
  DOCUMENTATION
  type = list(object({
    subnets          = list(string)
    security_groups  = optional(list(string))
    assign_public_ip = optional(bool)
  }))
  default = []
}

variable "service_ordered_placement_strategy" {
  description = <<-DOCUMENTATION
  Service level strategy rules that are taken into consideration during task placement. List from top to bottom in order of precedence. Updates to this configuration will take effect next task deployment unless force_new_deployment is enabled. The maximum number of ordered_placement_strategy blocks is 5.
  type (Required, string) : Type of placement strategy. Must be one of: `binpack`, `random`, or `spread`
  field (Optional, string) :  For the `spread` placement strategy, valid values are `instanceId` (or host, which has the same effect), or any platform or custom attribute that is applied to a container instance. For the `binpack` type, valid values are `memory` and `cpu`. For the `random` type, this attribute is not needed.
  DOCUMENTATION
  type = list(object({
    type  = string
    field = optional(string)
  }))
  default = []
}

variable "service_placement_constraints" {
  description = <<-DOCUMENTATION
  Rules that are taken into consideration during task placement. Updates to this configuration will take effect next task deployment unless force_new_deployment is enabled. Maximum number of placement_constraints is 10.
  type (Required, string) : Type of constraint. The only valid values at this time are `memberOf` and `distinctInstance`.
  expression (Optional, string) : Cluster Query Language expression to apply to the constraint. Does not need to be specified for the `distinctInstance` type.
  DOCUMENTATION
  type = list(object({
    type       = string
    expression = optional(string)
  }))
  default = []

  validation {
    condition = alltrue([
      for o in var.service_placement_constraints : contains(["memberOf", "distinctInstance"], o.type)
    ])
    error_message = "The service_placement_constraints name should be `memberOf` or `distinctInstance`."
  }
}

variable "service_platform_version" {
  description = "Platform version on which to run your service. Only applicable for launch_type set to FARGATE. Defaults to LATEST."
  type        = string
  default     = null
}

variable "service_propagate_tags" {
  description = "Specifies whether to propagate the tags from the task definition or the service to the tasks. The valid values are SERVICE and TASK_DEFINITION."
  type        = string
  default     = "SERVICE"

  validation {
    condition     = can(regex("SERVICE|TASK_DEFINITION", var.service_propagate_tags))
    error_message = "The “var.service_propagate_tags” does not match SERVICE or TASK_DEFINITION."
  }
}

variable "service_scheduling_strategy" {
  description = "Scheduling strategy to use for the service. The valid values are REPLICA and DAEMON."
  type        = string
  default     = "REPLICA"

  validation {
    condition     = can(regex("REPLICA|DAEMON", var.service_scheduling_strategy))
    error_message = "The “var.service_scheduling_strategy” does not match REPLICA or DAEMON."
  }
}

variable "service_service_registries" {
  description = <<-DOCUMENTATION
  Service discovery registries for the service. The maximum number of service_registries blocks is 1
  registry_arn   (Required, string) : ARN of the Service Registry. The currently supported service registry is Amazon Route 53 Auto Naming Service(`aws_service_discovery_service`)
  port           (Optional, number) : Port value used if your Service Discovery service specified an SRV record.
  container_port (Optional, number) : Port value, already specified in the task definition, to be used for your service discovery service.
  container_name (Optional, string) : Container name value, already specified in the task definition, to be used for your service discovery service.
  DOCUMENTATION
  type = list(object({
    registry_arn   = string
    port           = optional(number)
    container_port = optional(number)
    container_name = optional(string)
  }))
  default = []
}

variable "service_tags" {
  description = "Key-value map of resource tags. If configured with a provider default_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level."
  type        = map(string)
  default     = {}
}

variable "service_task_definition" {
  description = <<-DOCUMENTATION
  Family and revision (family:revision) or full ARN of the task definition that you want to run in your service.
  Required unless using the EXTERNAL deployment controller.
  If a revision is not specified, the latest ACTIVE revision is used
  DOCUMENTATION
  type        = string
  default     = null
}

variable "service_wait_for_steady_state" {
  description = "If true, Terraform will wait for the service to reach a steady state (like aws ecs wait services-stable) before continuing. Default false."
  type        = bool
  default     = false
}
